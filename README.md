# junit-report-gl 
[![pipeline status](https://gitlab.com/MihajloNesic/junit-report-gl/badges/master/pipeline.svg)](https://gitlab.com/MihajloNesic/junit-report-gl/pipelines) [![coverage report](https://gitlab.com/MihajloNesic/junit-report-gl/badges/master/coverage.svg)](https://gitlab.com/MihajloNesic/junit-report-gl/-/commits/master)

Automatically generate test coverage report using GitLab CI/CD.

[VIEW THE REPORT](https://mihajlonesic.gitlab.io/junit-report-gl/)

This project is part of my blog post about comparing GitLab and GitHub DevOps tools. Read about it [here](https://mihajlonesic.gitlab.io/archive/comparing-gitlab-and-github/).
